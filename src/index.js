import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import Accueil from './containers/Accueil';
import Moi from './containers/Moi';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory({
    basename: '/',
    forceRefresh: false, // Set true to force full page refreshes
    keyLength: 6, // The length of location.key
    hashType: 'slash', // The hash type to use (see below)
  // A function to use to confirm navigation with the user (see below)
  getUserConfirmation: (message, callback) => callback(window.confirm(message))
});

class MyComponent extends Component {
    constructor(){
        super();
        this.unlisten = history.listen(this.locationChange);
        this.page = {
            max:0,
            cur:0,
            anim:{
                dur:500,
                cur:'ease'
            }
        };
        this.wheel ={off:false,sum:0};
        this.state = {
            
        };
        //Refs
        this.pages = React.createRef();

        //Zone
        const sw = {
            onSwipeUp:() => this.setPage(this.page.cur+1),
            onSwipeDown: () => this.setPage(this.page.cur-1)
        }
        const model = {
            attribs:{
                swipes:sw,
                anim:this.page.anim,
                updateSlideUrl: this.updateSlideUrl,
            },
            ref:null
        }
        this.all = [
            {
                ...model,
                attribs:{
                    ...model.attribs,
                    name:'Accueil',
                },
                comp: Accueil,
            },
            {
                ...model,
                attribs:{
                    ...model.attribs,
                    name:'Moi',
                    background:'bg1l.jpg'
                },
                comp: Moi
            }
        ];
    }
    initLinks(){
        let two = {};
        let one = this.all.map(e=>{
            const k = e.attribs.name;
            two[k] = e.ref.current.getSlideNames();
            return k;
        });
        this.links = {
            one: {...one},
            two: {...two}
        }
        let {pathname} = history.location;
        pathname = pathname.split('/').filter(s=>s.length>0);
        if(pathname[0]){
            one = one.indexOf(pathname[0]);
            if(pathname[1]){
                two = two[pathname[0]].indexOf(pathname[1]);
                return this.setPage(one,two,false)
            }
            return this.setPage(one,undefined,false);
        }
        else{
            history.push(one[0]);
        }
    }

    locationChange(location,action){
        //console.log(location);
    }
    componentDidMount = () => {
        window.addEventListener('wheel', this.handleOnWheel);
        this.page.max = this.pages.current.querySelectorAll("#pages>.Page").length;
        this.pages.current.style.transition = `transform ${this.page.anim.dur}ms ${this.page.anim.cur}`;
        this.initLinks();
        console.log(history.location.pathname);
    }
    componentWillUnmount = () => {
        window.removeEventListener('wheel', this.handleOnWheel);
    }

    setPage = (page,slide,push=true,animSlide=false) =>{
        if(page===undefined || page>=this.page.max || page<0) return;
        const {one} = this.links;
        this.page.cur = page;
        this.pages.current.style.transform = 'translateY('+this.page.cur*-100+'vh)';
        if(slide!==undefined){
            this.all[page].ref.current.setSlide(slide,animSlide);
            this.updateSlideUrl(slide);
        }
        else if(push) this.doUrl(0,one[page]);
    }


    updateSlideUrl = (slide) =>{
        const {one,two} = this.links;
        const {cur} = this.page;
        if(slide>0)this.doUrl(0,one[cur]+'/'+two[one[cur]][slide]);
        else this.doUrl(0,one[cur]);
    }

    doUrl = (place,name) => {
        while(1){
            let h = history.location.pathname.split('/').filter(s=>s.length>0);
            history.push('');
            if(h.length<=place) break;
        }
        history.push(name);
    }

    handleOnWheel = (ev) => {
        if(this.wheel.off) return;
        this.wheel.sum += ev.deltaY;
        if(this.wheel.sum>=53 || this.wheel.sum<=-53){
            this.setPage(this.page.cur+this.wheel.sum/Math.abs(this.wheel.sum));
            this.wheel.sum = 0;
            this.wheel.off = true;
            setTimeout(()=>this.wheel.off=false,this.page.anim.dur);
        }
    }
 
    render() {
        console.log("render");
        return (
            <div id="portfolio">
                <div ref={this.pages} id="pages">
                    {
                        this.all.map((obj,i) => {
                            obj.ref = React.createRef();
                            return (
                                <obj.comp ref={obj.ref} key={i} {...obj.attribs}  />
                                );
                        })
                    }
                </div>
            </div>
            
        );
    }
}
 
ReactDOM.render(<MyComponent/>, document.getElementById('root'));