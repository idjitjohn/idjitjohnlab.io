const accueil = {
    vars : {
        name:'Marson John LAZA',
        title:'Développeur Full-Stack',
        citation:'Ne jamais confondre mouvement et progrès',
        licons:[]
    },
    cards: [
        {
            image:'bg1.jpg',
            title:'Marson? C\'est qui?',
            description:'Si ça vous dit de **savoir** qui je suis? C\'est par ici.\nCliquez, je ne mord pas ...',
            licons:[]
        },
        {
            image:'bg2.jpg',
            title:'Mes projets',
            description:'Sur quoi je travavaille? Vous êtes sûr que ça vous interesse?\nBahhh, c\'est par ici ...',
            licons:[]
        },
        {
            image:'bg3.jpg',
            title:'Me contacter',
            description:'Si vous desirez me contacter',
            'licons':["g.png","t.png","l.png","s.png","gh.png","f.png"],
        },
        {
            image:'bg5.jpg',
            title:'Ma gelerie perso',
            description:'Je ne sais pas vous, mais, moi j\'ai toujours adoré les galeries. Allez une tournée, je vous invite!',
            licons:[]
        },
    ]
}

export default accueil;