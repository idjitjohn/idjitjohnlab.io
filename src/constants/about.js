const about =
[
{
  navigation:{
    blanc:true,
    text:"Je suis **Marson (Lj)**, développeur freelance **depuis 2015**.\n"+
    "C'est **ma passion** et bientôt **mon travail**, donc ... quoi? J'ecris trop?\n"+
    "Ok ... Je vous invite tout de suite à **me connaitre** mieux ..."
  },
  article:{
    text:'Puzzle01'
  }
},
{
  name:'Présentations',
  code:'+',
  navigation:{
    message:"Bla bla bla!",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Salut, comme vous l'avez surement remarqué avec ma citation favorite, j'aime apprendre de nouvelles choses et m'améliorer dans n'importe quelle domaine.\nJe suis dans le monde du developpement depuis environ 3ans, je suis aussi bien à l'aise en developpement **backend qu'en frontend** et en design.\nJ'ai déjà participer à plusieurs projets et compétitions que ce soit dans le domaine de l'informatique ou d'autres domaines."
  }
},
{
  name:'Développement',
  code:'</DEV?>',
  navigation:{
    message:"Dev dev dev...",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
},
{
  name:'Voyages',
  navigation:{
    message:"Let's go to...",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
},
{
  name:'Famille',
  navigation:{
    message:"Sweet home...",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
},
{
  name:'Hobbies',
  navigation:{
    message:"Play with me...",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
},
{
  name:'CV',
  navigation:{
    message:"Compétences++;",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
},
{
  name:'Waouhhh',
  navigation:{
    message:"Sweet home...",
    image:'moi.jpg',
  },
  article:{
    blanc:true,
    text:"Vous pouvez voir ici voir mes langages, technologies et frameworks favorits.\nJ'en ai pas fini, il y a encore des cases à remplir."
  }
}

];

export default about;