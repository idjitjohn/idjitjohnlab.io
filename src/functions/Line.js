import React,{Fragment} from 'react';

const Bold = props => {
    const tab = props.text.split('**');
    let ret = tab.map( (line,i) => {
    return i%2===1? (<span className='gg' key={i}>{line}</span>):(<Fragment key={i}>{line}</Fragment>);
    });
    return ret;
}

const Line = props => {
    const tab = props.text.split('\n');
    let ret = tab.map( (line,i) => {
        let br = i<tab.length-1?(<br/>):(<></>);
        return (<Fragment key={i}><Bold text={line}/>{br}</Fragment>);
    });
    return ret;
}

const Article = props => (<span className='article'><Line {...props}></Line></span>);

export default {Line:Line,Article:Article};