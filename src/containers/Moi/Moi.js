import React,{Component} from 'react';
import Slide from '../../components/Slide';
import Const from '../../constants'
import Puzzle from '../../components/Puzzle';
import Page from '../Page';
import './Moi.scss';

class Moi extends Component{
    constructor(){
        super();
        this.page = React.createRef();
        this.names = [];
    }
    setSlide = slide => {
        this.page.current.setSlide(slide);
    }
    componentDidMount(){
        //this.setSlide(0);
    }
    render(){
        return (<Page ref={this.page} {...this.props} content = {this.content()}></Page>)
    }
    getName(abt){
        if(!abt || !abt.name) return undefined;
        else return abt.name;
    }
    content(){
        const { about } = Const;
        const num = {0:'first',1:'second',len:'last'};
        const tiles = about.map((el,i)=>({text: el.name, code:el.code,index:i})).filter((el)=>el.text);

        console.log(tiles);
        
        about[0].article = (<Puzzle setSlide={this.setSlide} type='about' tiles={tiles} background='bg1.jpg'>Salut</Puzzle>);
        return (
            <>
            {
                about.map((abt,i) => {
                    this.names.push(abt.name);
                    if(i>0){
                        let navs = [];
                        navs.push({
                            name: this.getName(about[i-1]),
                            link:()=>()=>{this.page.current.prev()},

                        });
                        navs.push({
                            name:this.getName(about[i+1]),
                            link:()=>()=>{this.page.current.next()},
                        });
                        abt.navigation.navs = navs;
                    }
                    return (<Slide cls={num[i]} {...abt} key={i}/>);
                })
            }
            </>
        );
    }
    getSlideNames = () => this.names
}
export default Moi;