import React,{Component} from 'react';
import './Accueil.css';
import Card from '../../components/Card';
import Const from '../../constants';
import Page from '../Page';

class Accueil extends Component{
    constructor(){
        super();
        this.page = React.createRef();
    }

    render(){
        return (<Page ref={this.page} {...this.props} content = {this.content()}></Page>);
    }
    content(){
        const {cards,vars} = Const.accueil;
        return (
            <div className='lj-full'>
                <div className="homehead">
                    <div className="profilToph"></div>
                    <div className="about">
                        <div className="name">{vars.name}</div>
                        <div className="title">{vars.title}</div>
                        <div className="citation">"{vars.citation}"</div>
                    </div>
                </div>
                <div className="plan">
                    <div className="backplan"></div>
                    {cards.map((card,i)=>(<Card key={i} {...card} />))}
                </div>
            </div>
        );
    }
    setSlide = slide => {
        this.page.current.setSlide(slide);
    }
    getSlideNames = () => []
} 

export default Accueil;