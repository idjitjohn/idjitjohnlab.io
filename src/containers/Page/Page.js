import React,{Component} from 'react';
import Swipe from 'react-easy-swipe';
import './Page.scss'

class Page extends Component
{
    constructor(){
        super();
        this.ref = React.createRef();
        this.home = React.createRef();
        this.backflou = React.createRef();
        this.nb = 1;
        this.cur = 0;
    }
    setSlide = (slide, trans=true) => {
        if(this.nb===1 || slide<0 || slide>=this.nb) return;
        const {cur,dur} = this.props.anim;
        let style = this.ref.current.swiper.style,
        homestyle = this.home.current.style,
        floustyle = this.backflou.current.style;

        floustyle.transition = homestyle.transition = style.transition =
                trans ? (`transform ${dur}ms ${cur}`):'';
        style.transform = `translateX(${-100*slide}vw)`;

        homestyle.transform = floustyle.transform = `translateX(${100*slide}vw)`;
        floustyle.transform += ` scale(1.1)`;
        homestyle.transform += ` scale(${slide>0?1:0})`;
        this.cur = slide;
        this.props.updateSlideUrl(slide);
    }
    next = ()=>{
        this.setSlide(this.cur+1)
    }
    prev = () => {
        this.setSlide(this.cur-1)
    }
    componentDidMount(){
        if(this.ref.current){
            const me = this.ref.current.swiper;
            const nb = me.querySelectorAll('.Slide').length;
            if(nb!==0) {
                this.nb = nb;
                me.className += " parent";
            }
            me.style.width = this.nb*100+'vw';
        }
    }
    render(){
        return (<Swipe  {...this.props.swipes} ref={this.ref} className={"Page "+this.props.name}>
            <div ref={this.home} className='home' onClick={()=>this.setSlide(0)}></div>
            {this.getBackground()}
            {this.props.content}
        </Swipe>)
    }

    getBackground(){
        const {background} = this.props;
        if(background){
            const img = require('../../assets/img/'+background);
            return (<img alt={background} ref={this.backflou} className="backflou"  src={img}/>);
        }
            
        else return null;
    }
}

export default Page;