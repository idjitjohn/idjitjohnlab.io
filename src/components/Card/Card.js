import React,{Component} from 'react';
import './Card.css';
import F from '../../functions/Line';
import arrow from '../../assets/img/arrow.png';

class Card extends Component{
    render(){
        const back_style = {
            backgroundImage:"url("+require('../../assets/img/'+this.props.image)+")",
        };
        return(
            <div className='lj-bdg'>
                <div className='lj-bdg-image'>
                    <div className='back' style={back_style}>
                        <div className='masque'></div>
                    </div>
                </div>
                <div className='lj-bdg-info'>
                    <div className='lj-bdg-title'>{this.props.title}</div>
                    <div className='lj-bdg-desc'>
                        <F.Line text={this.props.description}/>
                        {
                            (this.props.licons.length>0) ?
                                (<div className='licons'>
                                    {this.props.licons.map((licon,i)=>(
                                        <img alt={licon} className='licon' key={i} src={require('../../assets/img/'+licon)} />
                                    ))}
                                </div>):false
                            
                        }
                    </div>

                </div>
                <img alt={arrow} className='arrow' src={arrow}/>
            </div>
        );
    }


}
export default Card;