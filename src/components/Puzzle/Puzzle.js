import React,{Component} from 'react';
import './Puzzle.scss';

class Puzzle extends Component{
    constructor(){
        super();
        this.models = {
            about:{
                nb:20,
                cls:'blanc',
                ok:(val)=>[0,5,7,8,13,15,17].indexOf(val)>=0,
                mob:2,
            },
            projet:{
                cls:'',
                ok:(val)=>true,
                nb:8,
                mob:4,
            }
        }
    }
    render(){
        const {background,type,} = this.props;
        const style = {
            backgroundImage:'url('+require('../../assets/img/'+background)+')',
        }
        return(<div className={"Puzzle "+type} style={style}>
        {this.buildModel()}
        </div>);
    }

    buildModel = () => {
        const {type,tiles} = this.props;
        const {nb,cls,ok,mob} = this.models[type];
        let arr = [];
        for(let i=0,j=0;i<nb;i++){
            let cl = (ok(i)?cls:'')+((nb-i<=mob?' mob':''));
            if(ok(i) && tiles[j]){
                let {text,code,index} = tiles[j];
                if(!code) code = text;
                arr.push(<a key={i} onClick={()=>this.setSlide(index)} className={cl}><span><span>{code}</span></span></a>);
                j++;
            }
            else arr.push(<a key={i} className={cl}></a>);
        }
        return arr;
    }

    setSlide(index){
        this.props.setSlide(index);
    }
}

export default Puzzle;