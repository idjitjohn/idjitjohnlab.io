import React,{Component} from 'react';
import './Navigation.scss'

class Navigation extends Component{
    render(){
        const {message, image, navs} = this.props;
        const back = {backgroundImage:"url("+require('../../assets/img/'+image)+")"};
        return (
            <>
            <div className="profil" style={back}>
                <div className="message"><span>{message}</span></div>
            </div>
            <div className="navigation">
                {navs.map((nav,i)=>{
                    if(!nav) return null;
                    if(i===0) return nav.name?(<a onClick={nav.link()} key={i}>&lt; {nav.name}</a>):null;
                    else return nav.name?(<a onClick={nav.link()} key={i}>{nav.name} &gt;</a>):null;
                })}
            </div>
            </>
        );
    }
}
export default Navigation;