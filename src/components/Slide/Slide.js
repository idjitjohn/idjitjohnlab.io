import React,{Component} from 'react';
import './Slide.scss';
import F from '../../functions/Line';
import Navigation from'../Navigation';

class Slide extends Component{
    render(){
        const {cls,article,navigation,reverse} = this.props;
        let Art = article;
        if(Art.text!==undefined) Art = (<F.Article {...article}/>);
        let Cont = navigation.message===undefined ? F.Article : Navigation;
        return (<div className={'Slide'+(reverse?' reverse':'')+" "+cls}>
            <div className={"slide-head"+(navigation.blanc?' blanc':'')}>
                <Cont {...navigation}/>
            </div>
            <div className={"slide-body"+(article.blanc?' blanc':'')}>
                {Art}
            </div>
        </div>)
    }
}

export default Slide;